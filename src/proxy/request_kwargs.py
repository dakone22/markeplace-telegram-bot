def get_request_kwargs(proxy_url: str, need_cert: bool = True, username: str = None, password: str = None):
    urllib3_proxy_kwargs = {
        'cert_reqs': 'CERT_REQUIRED',
    }

    if not need_cert:
        urllib3_proxy_kwargs['cert_reqs'] = 'CERT_NONE'
        urllib3_proxy_kwargs['assert_hostname'] = 'False'

    if username:
        urllib3_proxy_kwargs['username'] = username

    if password:
        urllib3_proxy_kwargs['password'] = password

    request_kwargs = {
        'proxy_url': proxy_url,
        'urllib3_proxy_kwargs': urllib3_proxy_kwargs,
    }

    return request_kwargs
