import os

# Proxy VS RosKomNadzor
from settings.main import BASE_DIR

USE_PROXY = True

# Path to file with proxy list with format:
# <type>://<ip>:<port>/
# Example: socks4://1.179.180.98:4145/
PROXY_FILE = os.path.join(BASE_DIR, 'rsc', 'proxy_list.txt')
