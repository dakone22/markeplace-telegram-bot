import json
from logging import getLogger
from math import ceil

import requests
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import Updater, CallbackContext, Filters

from settings.api_server import API_SERVER
from src.register import decorators as register
from src.register.variables import HANDLERS

logger = getLogger(__name__)


@register.error_handler
def error(update: Updater, context: CallbackContext):
    """ Log Errors caused by Updates. """
    logger.warning('Update "%s" caused error "%s"', update, context.error)
    update.message.reply_text("Ошибка: {}".format(context.error))


@register.message_handler(Filters.text)
def echo(update: Updater, context: CallbackContext):
    """ Echo the user message. """
    update.message.reply_text(update.message.text)


@register.command_handler('start')
def start(update: Updater, context: CallbackContext):
    """Начальная команда"""
    update.message.reply_text('Hi!')


@register.command_handler('help', "Список доступных команд")
def help(update: Updater, context: CallbackContext):
    lines = [
        '*Доступные команды*:',
    ]

    for cmd, func in HANDLERS['COMMAND_HANDLERS'].items():
        dscr = func.__command_description
        s = f"/{cmd}"

        if dscr:
            s += f" — {dscr.strip()}"

        lines.append(s)

    update.message.reply_markdown_v2('\n'.join(lines))


@register.command_handler('about')
def about(update: Updater, context: CallbackContext):
    """Информация о боте"""

    msg = "[GitLab-репозиторий бота](https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/)\n" \
          "Там Вы можете найти мой токен (безопасность 100lvl)"

    update.message.reply_markdown_v2(msg)


@register.command_handler('list')
def list(update: Updater, context: CallbackContext, page=1):
    """Список товаров\. Используйте `/list <page>` для конкретной страницы"""

    MAX_ON_PAGE = 3
    PRODUCT_LIST_URL = API_SERVER['URL'] + "api/product"
    PRODUCT_DETAIL_URL = API_SERVER['URL'] + "catalogue/{slug}"

    response = requests.get(PRODUCT_LIST_URL, data={'format': 'json'},
                            headers={'Authorization': f'Token {API_SERVER["AUTH_TOKEN"]}', })
    product_list = json.loads(response.text)[::-1]

    pages = ceil(len(product_list) / MAX_ON_PAGE)
    if context.args:
        if not context.args[0].isnumeric() or not (1 <= int(context.args[0]) <= pages):
            update.message.reply_markdown_v2(f"Используйте `/list <page>`, где `page` – число от `1` до `{pages}`!")
            return
        page = int(context.args[0])

    header = 'Список товаров{}:'.format(
        f' — страница <b>{page}</b> из <b>{pages}</b>' if pages > 1 else ''
    )

    lines = [
        header,
    ]

    for index, product in enumerate(product_list[(page - 1) * MAX_ON_PAGE: page * MAX_ON_PAGE]):
        title = product['title']
        description = product['description'].replace('<p>', '').replace('</p>', '')
        slug = product['slug']

        s = "<b>{index}</b>: <a href='{url}'>{text}</a>\n".format(
            index=(page - 1) * MAX_ON_PAGE + index + 1,
            text=title,
            url=PRODUCT_DETAIL_URL.format(slug=slug)
        )
        if description:
            s += f"{description}\n"

        lines.append(s)

    button_list = [
        # InlineKeyboardButton("Выход", callback_data="Exit"),
    ]
    if page > 1:
        button_list = [InlineKeyboardButton("<<<", callback_data=f"list {page - 1}"), ] + button_list
    if page < pages:
        button_list = button_list + [InlineKeyboardButton(">>>", callback_data=f"list {page + 1}"), ]

    reply_markup = InlineKeyboardMarkup([button_list])

    context.bot.send_message(
    # update.message.reply_html(
        chat_id=(update.message or update.callback_query.message).chat_id,
        text='\n'.join(lines),
        parse_mode=ParseMode.HTML,
        disable_web_page_preview=True,
        reply_markup=reply_markup
    )


@register.callback_query_handler(None)
def all_callback(update, context):
    query = update.callback_query
    cmd, *args = query.data.split(maxsplit=1)

    if cmd == "list":
        list(update, context, int(args[0]))
