from settings.main import LOGGING, TOKEN, USE_PROXY
from src.bot.controller import BotController
from src.bot.model import BotModel
from src.proxy.generator import get_proxy_generator


def logging():
    import logging
    logging.basicConfig(format=LOGGING["FORMAT"], level=eval(LOGGING["LEVEL"]))  # TODO: loader

    logger = logging.getLogger(__name__)
    return logger


def main():
    if LOGGING["ENABLE"]:
        logging()

    bot = BotModel(TOKEN)
    ctrl = BotController(bot, get_proxy_generator())

    ctrl.start(USE_PROXY)


if __name__ == '__main__':
    main()
