from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler
from logging import getLogger

from settings.main import COMMAND_FILES
from src.register.variables import HANDLERS

logger = getLogger(__name__)


class BotModel:
    def __init__(self, token):
        self.token = token

    def create_updater(self, use_context=True, request_kwargs=None):
        self.updater = Updater(self.token, use_context=use_context, request_kwargs=request_kwargs)
        return self.updater

    def register_handlers(self):
        for file in COMMAND_FILES:  # TODO: loader
            __import__(file)

        # on different commands - answer in Telegram
        for command, func in HANDLERS["COMMAND_HANDLERS"].items():
            try:
                self.updater.dispatcher.add_handler(CommandHandler(command, func))
            except ValueError:
                logger.warning("ERROR IN REGISTRATION: ", command, func)

        # on noncommand i.e message - echo the message on Telegram
        for filter, func in HANDLERS["MESSAGE_HANDLERS"]:
            self.updater.dispatcher.add_handler(MessageHandler(filter, func))

        # log all errors
        if HANDLERS["ERROR_HANDLER"]:
            self.updater.dispatcher.add_error_handler(HANDLERS["ERROR_HANDLER"])

        # callback query handlers (for keyboard menu buttons)
        if isinstance(HANDLERS["CALLBACK_QUERY_HANDLERS"], dict):
            for pattern, func in HANDLERS["CALLBACK_QUERY_HANDLERS"].items():
                self.updater.dispatcher.add_handler(CallbackQueryHandler(func, pattern=pattern))
        else:
            self.updater.dispatcher.add_handler(CallbackQueryHandler(HANDLERS["CALLBACK_QUERY_HANDLERS"]))

    def polling(self):
        self.updater.start_polling()

    def webhook(self, ip, port, url_path, webhook_url, certificate):
        self.updater.start_webhook(listen=ip, port=port, url_path=url_path)
        self.updater.bot.set_webhook(webhook_url=webhook_url, certificate=certificate)
