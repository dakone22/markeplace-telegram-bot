# marketplace-telegram-bot

Телеграмм-бот для проекта [marketplace](https://gitlab.informatics.ru/2019-2020/mytischi/ms104/marketplace)

## Установка и запуск

1. Склонировать, настроить виртуальную среду (по вкусу)
2. Установить необходимые пакеты
```bash
pip install -r requirements.txt
```
3. Запусть `main.py`


## Список изменений
Список изменений можно найти в файле [CHANGELOG.md](CHANGELOG.md)