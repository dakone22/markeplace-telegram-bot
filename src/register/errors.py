class AlreadyRegisteredException(Exception):
    pass


class AlreadyRegisteredWarning(Warning):
    pass
