import os
from logging import getLogger
from time import time

from telegram.error import NetworkError
from telegram.vendor.ptb_urllib3.urllib3.exceptions import MaxRetryError

from settings.main import RESOURCE_DIR
from settings.telegram import METHOD_WEBHOOK, METHOD
from src.bot.model import BotModel
from src.proxy.request_kwargs import get_request_kwargs

logger = getLogger(__name__)


class BotController:
    EXCEPTIONS = (NetworkError, MaxRetryError)
    PROXY_REPORT_DIR = os.path.join(RESOURCE_DIR, 'proxy-reports')

    def __init__(self, bot_model: BotModel, proxy_generator=None):
        self.bot_model = bot_model
        self.proxy_generator = proxy_generator

    def listen(self, method: str):
        if method is "POLLING":
            self.bot_model.polling()
        elif method is "WEBHOOK":
            ip = METHOD_WEBHOOK['LISTEN']
            port = METHOD_WEBHOOK['PORT']
            url_path = METHOD_WEBHOOK['URL_PATH']
            webhook_url = METHOD_WEBHOOK['WEBHOOK_URL']
            certificate = METHOD_WEBHOOK['CERTIFICATE']

            self.bot_model.webhook(ip, port, url_path, webhook_url, certificate)

    def launch(self, method: str, use_context: bool = True, request_kwargs=None):
        # Create the Updater and pass it your bot's token.
        # Make sure to set use_context=True to use the new context based callbacks
        # Post version 12 this will no longer be necessary
        self.bot_model.create_updater(use_context=use_context, request_kwargs=request_kwargs)

        # Start the BotModel
        self.listen(method)
        logger.info("Bot listen started")

        # Get the dispatcher to register handlers
        self.bot_model.register_handlers()
        logger.info("Handlers registered")

        # Run the bot until you press Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        self.bot_model.updater.idle()

    def protected_launch(self, method: str, fail_message: str, request_kwargs=None):
        try:
            self.launch(method, request_kwargs=request_kwargs)
        except self.EXCEPTIONS:
            logger.warning(fail_message)

    def start(self, use_proxy: bool = False):
        if use_proxy and not self.proxy_generator:
            raise ValueError("Need proxy generator!")

        self.running = True

        if not use_proxy:
            while True:
                self.protected_launch(METHOD, "Bot failed [NO PROXY]. Retrying connection...")

        else:
            for proxy in self.proxy_generator:
                request_kwargs = get_request_kwargs(proxy_url=proxy)
                time_start = time()
                self.protected_launch(METHOD, f"Bot failed [PROXY: {proxy}]. Retrying connection...",
                                      request_kwargs=request_kwargs)
                self.proxy_report(proxy, time() - time_start)

    def proxy_report(self, proxy: str, time: float):
        proxy = proxy
        proxy_file = os.path.join(self.PROXY_REPORT_DIR, f"proxy-{proxy.replace('/', '').replace(':', '-')}.txt")

        if not os.path.exists(self.PROXY_REPORT_DIR):
            os.makedirs(self.PROXY_REPORT_DIR, exist_ok=True)

        if not os.path.exists(proxy_file):
            with open(proxy_file, 'w') as f:
                print(f"PROXY REPORT {proxy}. WORKING TIME (in secs):", file=f)

        with open(proxy_file, 'a') as f:
            print(round(time, 2), file=f)
