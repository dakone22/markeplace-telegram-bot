# Changelog — Список изменений
Основано на  [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]


## [0.3.0] - 2020-06-01
### Changed
- Код бота разбит на несколько файлов.

### Removed
- Старый код бота удалён.


## [0.2.2] - 2020-05-30
### Added
- Добавлен `CHANGELOG.md`.


## [0.2.1] - 2020-05-30
### Changed
- Изменён `README.md`.


## [0.2.0] - 2020-05-29
### Added
- Проект в [GitLab](https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot)
- Полная разбивка проекта на части
- Файлы `requirements.txt`, `README.md`, `.gitignore`

### Removed
- Первая версия бота в одном файле


## [0.1.0] - 2020-04-13
### Added
- Первая версия бота в одном файле


[Unreleased]: https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/
[0.3.0]: https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/-/commit/193f51078fda1643c5da6ace8ecf02fff6ff60e6
[0.2.2]: https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/-/commit/0a01ce4b860df11765ed0eddcc66bf2389ad4741
[0.2.1]: https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/-/commit/f46958e9afc10bd638eea22ae476d450070bed00
[0.2.0]: https://gitlab.informatics.ru/dakone22/marketplace-telegram-bot/-/commit/fb63a2fe7a615be9fcc7385004d96dfe316f45de
[//]: # (https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.0.1)
