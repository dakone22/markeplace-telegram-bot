import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
RESOURCE_DIR = os.path.join(BASE_DIR, 'rsc')

LOGGING = {
    "ENABLE": True,
    "FORMAT": '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    "LEVEL": 'logging.INFO',
}

COMMAND_FILES = [
    'logic.main_commands',
]

from settings.telegram import *
from settings.proxy import *
from settings.api_server import *
